package com.example.cvdemo;

import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;

public class App 
{

    public static void main( String[] args ) throws URISyntaxException
    {
        String filepath = "/Users/hermesian/Desktop/lena.png";
    	resize(filepath);
        // smooth(filepath);
        // gray(filepath);
    }
    
    public static void resize(String filepath) {
    	IplImage source = cvLoadImage(filepath);
    	if (source != null) {
    		IplImage dest = cvCreateImage(cvSize(source.width() / 2, source.height() / 2), source.depth(), source.nChannels());
    		cvResize(source, dest, CV_INTER_NN);
    		// TODO: this is not working well.
    		// cvSave("half-" + Paths.get(filepath).getFileName().toString(), dest);
    		Mat target = new Mat(dest);
    		imwrite("half-" + Paths.get(filepath).getFileName().toString(), target);
    		cvRelease(source);
    		cvRelease(dest);
    	}
    }
    
    public static void smooth(String filename) {
    	IplImage image = cvLoadImage(filename);
    	if (image != null) {
    		cvSmooth(image, image);
    		// cvSave("smooth-" + Paths.get(filename).getFileName().toString(), image);
    		// cvSaveImage(filename, image);
    		Mat target = new Mat(image);
    		imwrite("smooth-" + Paths.get(filename).getFileName().toString(), target);
    		cvReleaseImage(image);
    	
    	}
    }
    
    public static void gray(String filename) {
    	Mat source = imread(filename);
    	Mat dest = new Mat(source.size(), CV_8UC1);
    	cvtColor(source, dest, COLOR_BGR2GRAY);
    	
    	// save processd image
    	imwrite("dest.png",dest);
    }
}
